/**
 * Created by chitra on 18/9/17.
 */

var myApp = angular
    .module('app', [
        'ui.router',
        'ngStorage',
        'ngMaterial',
        'ngAnimate',
        'ngAria',
        'ngMaterialDatePicker',
        'mdPickers'
    ]);

myApp.config(function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('signUp', {
            url: '/signUp',
            templateUrl: 'app/signUp/signUp.html',
            controller: 'SignUpController'
        })
        .state('login', {
            url: '/login',
            templateUrl: 'app/login/login.html',
            controller: 'LoginController'
        })
        .state('home', {
            abstract: true,
            url: '/home',
            templateUrl: 'app/home/home.html',
            controller: 'HomeController'
        })
        .state('home.pipeline', {
            url: '/pipeline',
            templateUrl: 'app/pipeline/pipeline.html',
            controller: 'PipelineController'
        })
        .state('home.active', {
            url: '/active',
            templateUrl: 'app/active/active.html',
            controller: 'ActiveController'
        })
        .state('home.receivables', {
            url: '/receivables',
            templateUrl: 'app/receivables/receivables.html',
            controller: 'ReceivablesController'
        })
        .state('home.receiptsAndPayments', {
            url: '/receiptsAndPayments',
            templateUrl: 'app/receiptsAndPayments/receiptsAndPayments.html',
            controller: 'ReceiptsController'
        })
        .state('home.customers', {
            url: '/customers',
            templateUrl: 'app/customers/customers.html',
            controller: 'CustomersController'
        })
        .state('home.sales', {
            url: '/sales',
            templateUrl: 'app/sales/sales.html',
            controller: 'SalesController'
        })
        .state('home.banks', {
            url: '/banks',
            templateUrl: 'app/banks/banks.html',
            controller: 'BanksController'
        })
        .state('home.legal', {
            url: '/legal',
            templateUrl: 'app/legal/legal.html',
            controller: 'LegalController'
        })
        .state('home.executives', {
            url: '/legal',
            templateUrl: 'app/executives/executives.html',
            controller: 'ExecutivesController'
        })
        .state('home.auctions', {
            url: '/auctions',
            templateUrl: 'app/auctions/auctions.html',
            controller: 'AuctionsController'
        });
        


    $urlRouterProvider.otherwise('/login');
});

myApp.filter('capitalize', function() {
    return function(input, all) {
        var reg = (all) ? /([^\W_]+[^\s-]*) */g : /([^\W_]+[^\s-]*)/;
        return (!!input) ? input.replace(reg, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();}) : '';
    }
});

myApp.directive('errSrc', function() {
    return {
        link: function(scope, element, attrs) {
            element.bind('error', function() {
                if (attrs.src != attrs.errSrc) {
                    attrs.$set('src', attrs.errSrc);
                }
            });
        }
    }
});
