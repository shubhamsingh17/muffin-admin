/**
 * Created by chitra on 18/9/17.
 */

(function() {
    'use strict';
    angular
        .module('app')
        .controller('IndexController', IndexController);

    IndexController.$inject = ['$scope', '$rootScope', '$state','$localStorage'];

    function IndexController($scope, $rootScope, $state, $localStorage) {

        console.log($localStorage.userId);

        //Go To State
        $scope.goToLogin = function () {
            $state.go('login');
        };
        
        $scope.goToSignUp = function(){
            $state.go('signUp');
        };

        $scope.goToHomePage = function(){
            $state.go('home');
        };

        $scope.goToPipeline = function () {
            $state.go('home.pipeline');
        };

        $scope.goToActive = function () {
            $state.go('home.active');
        };

        $scope.goToReceivables = function () {
            $state.go('home.receivables');
        };

        $scope.goToReceiptsAndPayments = function () {
            $state.go('home.receiptsAndPayments');
        };
        
        $scope.goToAuctions = function () {
            $state.go('home.auctions');
        };

        $scope.goToCustomers = function () {
            $state.go('home.customers');
        };

        $scope.goToSales = function () {
            $state.go('home.sales');
        };

        $scope.goToBanks = function () {
            $state.go('home.banks');
        };
        
        $scope.goToLegal = function () {
            $state.go('home.legal');
        };

        $scope.goToExecutives = function () {
            $state.go('home.executives');
        };

    }

})();