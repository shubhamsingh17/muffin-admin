/**
 * Created by chitra on 18/9/17.
 */

(function() {
    'use strict';
    angular
        .module('app')
        .factory('network', network);

    network.$inject = ['$http','$localStorage'];

    function network($http,$localStorage) {
        var baseUrl = "http://localhost:9000/";
        // var baseUrl = "http://ec2-13-229-163-194.ap-southeast-1.compute.amazonaws.com:9000/";
        var imgUrl = "https://s3-ap-southeast-1.amazonaws.com/muffdata/";
        var network = {};

        //login
        network.loginApiCall = function(data) {
            console.log(baseUrl + 'api/muffin-office-users/login');

            return $http({
                method: 'POST',
                url: baseUrl + 'api/muffin-office-users/login',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: data
            });
        };

        //signup
        network.signUpApiCall = function(data) {
            return $http({
                method: 'POST',
                url: baseUrl + 'api/muffin-office-users/create',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: data
            });
        };

        // 	Get Banks
        network.getBanksApiCall = function() {
            return $http({
                method: 'GET',
                url: baseUrl + 'api/banks',
                headers: {
                    'Content-Type': 'application/json'
                }
            });
        };

        // Create Bank
        network.createBankApiCall = function(data) {
            return $http({
                method: 'POST',
                url: baseUrl + 'api/banks/newBank',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: data
            });
        };

        // Edit Bank
        network.editBankApiCall = function(data, bankId) {
            return $http({
                method: 'PUT',
                url: baseUrl + 'api/banks/' + bankId,
                headers: {
                    'Content-Type': 'application/json'
                },
                data: data
            });
        };

        // 	Get Legals
        network.getLegalsApiCall = function() {
            return $http({
                method: 'GET',
                url: baseUrl + 'api/legals',
                headers: {
                    'Content-Type': 'application/json'
                }
            });
        };

        // Add Legal
        network.addLegalApiCall = function(data) {
            return $http({
                method: 'POST',
                url: baseUrl + 'api/legals/newLegal',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: data
            });
        };

        // 	Get Tasks Legal
        network.getTasksApiCall = function(legalId) {
            console.log(baseUrl + 'api/tasks/' + legalId);
            return $http({
                method: 'GET',
                url: baseUrl + 'api/tasks/findTaskLegals/' + legalId,
                headers: {
                    'Content-Type': 'application/json'
                }
            });
        };

        // Add Task
        network.addTaskApiCall = function(data) {
            return $http({
                method: 'POST',
                url: baseUrl + 'api/tasks/create',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: data
            });
        };

        // 	Get Sales
        network.getSalesApiCall = function() {
            return $http({
                method: 'GET',
                url: baseUrl + 'api/sales',
                headers: {
                    'Content-Type': 'application/json'
                }
            });
        };

        // Add Sales
        network.addSalesApiCall = function(data) {
            return $http({
                method: 'POST',
                url: baseUrl + 'api/sales/create',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: data
            });
        };

        // 	Get Tasks Sales
        network.getSalesTasksApiCall = function(salesId) {
            console.log(baseUrl + 'api/tasks/' + salesId);
            return $http({
                method: 'GET',
                url: baseUrl + 'api/tasks/findTaskSales/' + salesId,
                headers: {
                    'Content-Type': 'application/json'
                }
            });
        };

        //Add muffins
        network.addNewMuffinApiCall = function(data) {
            console.log($localStorage.authToken);
            return $http({
                method: 'POST',
                url: baseUrl + 'api/muffins/',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: data
            });
        };

        // // 	Get available muffin
        // network.getAvailableMuffinsApiCall = function(userId) {
        //     return $http({
        //         method: 'GET',
        //         url: baseUrl + 'api/muffins-for-users/getAvailableMuffins/' + userId,
        //         headers: {
        //             'Content-Type': 'application/json'
        //         }
        //     });
        // };

        // 	Get available office muffin
        // network.getAvailableMuffinsApiCall = function(userId) {
        //     return $http({
        //         method: 'GET',
        //         url: baseUrl + 'api/muffins-for-users/allMuffinStates/' + userId,
        //         headers: {
        //             'Content-Type': 'application/json'
        //         }
        //     });
        // };

        network.getAvailableMuffinsApiCall = function(userId) {
            console.log(baseUrl + 'api/muffins/allMuffins');
            return $http({
                method: 'GET',
                url: baseUrl + 'api/muffins/allMuffins',
                headers: {
                    'Content-Type': 'application/json'
                }
            });
        };

        // 	Get Muffin Users
        network.getMuffinUsersApiCall = function() {
            return $http({
                method: 'GET',
                url: baseUrl + 'api/users/userList',
                headers: {
                    'Content-Type': 'application/json'
                }
            });
        };

        // 	Get Muffin  Office Users
        network.getOfficeUsersApiCall = function() {
            return $http({
                method: 'GET',
                url: baseUrl + 'api/muffin-office-users',
                headers: {
                    'Content-Type': 'application/json'
                }
            });
        };

        //Accept Decline muffins
        network.acceptDeclineApiCall = function(data) {
            console.log($localStorage.authToken);
            return $http({
                method: 'POST',
                url: baseUrl + 'api/muffins/state',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: data
            });
        };

        // 	Get Auctions
        network.getAuctionsApiCall = function() {
            return $http({
                method: 'GET',
                url: baseUrl + 'api/auctions',
                headers: {
                    'Content-Type': 'application/json'
                }
            });
        };

        // Receipts And Payment
        // Get All Payments
        network.getAllPaymentsApiCall = function(data) {
            return $http({
                method: 'POST',
                url: baseUrl + 'api/reports-auctionwon/new',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: data
            });
        };

        network.getAllUsersOfMuffinApiCall = function(muffinId) {
            return $http({
                method: 'GET',
                url: baseUrl + 'api/muffins-for-users/getMuffinMembers/' + muffinId,
                headers: {
                    'Content-Type': 'application/json'
                }
            });
        };

        // Reports
        // get late fee amount
        network.getLateFeeApiCall = function() {
            return $http({
                method: 'GET',
                url: baseUrl + 'api/reports-latefeecharges',
                headers: {
                    'Content-Type': 'application/json'
                }
            });
        };

        // get bounce fee amount
        network.getBounceFeeApiCall = function() {
            return $http({
                method: 'GET',
                url: baseUrl + 'api/reports-bouncecharges',
                headers: {
                    'Content-Type': 'application/json'
                }
            });
        };

        // get delayed payment interest amount
        network.getDelayedPaymentFeeApiCall = function() {
            return $http({
                method: 'GET',
                url: baseUrl + 'api/reports-delayedpaymentinterest',
                headers: {
                    'Content-Type': 'application/json'
                }
            });
        };

        // get instalment payable fee amount
        network.getInstalmentPayableFeeApiCall = function() {
            return $http({
                method: 'GET',
                url: baseUrl + 'api/reports-installmentpayable',
                headers: {
                    'Content-Type': 'application/json'
                }
            });
        };

        // get amount paid amount
        network.getAmountPaidFeeApiCall = function() {
            return $http({
                method: 'GET',
                url: baseUrl + 'api/reports-amountpaid',
                headers: {
                    'Content-Type': 'application/json'
                }
            });
        };

        // add Invite muffin
        network.addInvitedMuffinApiCall = function(data) {
            return $http({
                method: 'PUT',
                url: baseUrl + 'api/muffins-for-users/add/invite',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: data
            });
        };


        return network;
        
    }
})();
