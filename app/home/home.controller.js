/**
 * Created by chitra on 18/9/17.
 */

(function() {
    'use strict';
    angular
        .module('app')
        .controller('HomeController', HomeController);

    HomeController.$inject = ['$scope', '$state', '$timeout','$http','network','$localStorage','$mdSidenav'];

    function HomeController($scope, $state, $timeout, $http, network, $localStorage, $mdSidenav) {

        $scope.sideNavType = 'muffins';
        $scope.sideNavSubType = 'pipeline';

        $scope.selectType = function (type, subType) {
            $scope.sideNavType = type;
            $scope.sideNavSubType = subType;
            
            if(angular.equals(type, 'muffins') && angular.equals(subType, 'pipeline')){
                $scope.$parent.goToPipeline();

            }else if(angular.equals(type, 'muffins') && angular.equals(subType, 'active')){
                $scope.$parent.goToActive();

            }else if(angular.equals(type,'receivables') && angular.equals(subType, '')){
                $scope.$parent.goToReceivables();

            }else if(angular.equals(type,'receipts')){
                $scope.$parent.goToReceiptsAndPayments();

            }else if(angular.equals(type,'auction')){
                $scope.$parent.goToAuctions();

            }else if(angular.equals(type,'customers')){
                $scope.$parent.goToCustomers();

            }else if(angular.equals(type,'sales')){
                $scope.$parent.goToSales();

            }else if(angular.equals(type, 'misc') && angular.equals(subType, 'banks')){
                $scope.$parent.goToBanks();

            }else if(angular.equals(type,'misc') && angular.equals(subType, 'legal')) {
                $scope.$parent.goToLegal();
                
            }else if(angular.equals(type,'misc') && angular.equals(subType, 'executive')) {
                $scope.$parent.goToExecutives();

            }
        };
        
        $scope.showModal = function (modal) {
            if(angular.equals(modal,'step1')){
                $('.previousSanctionModal').appendTo("body").modal('show');

            }else if(angular.equals(modal,'step2')){
                $('.registrationModal').appendTo("body").modal('show');

            }else if(angular.equals(modal,'step3')){
                $('.commencementModal').appendTo("body").modal('show');

            }else if(angular.equals(modal,'step4')){
                $('.completionModal').appendTo("body").modal('show');

            }else if(angular.equals(modal,'addBank')){
                $('.addBankModal').appendTo("body").modal('show');

            }else if(angular.equals(modal, 'filter')){
                $('.filterModal').appendTo("body").modal('show');
            }
        };

        $scope.closeModal = function () {
            $('.previousSanctionModal').appendTo("body").modal('hide');

            $('.registrationModal').appendTo("body").modal('hide');

            $('.commencementModal').appendTo("body").modal('hide');

            $('.completionModal').appendTo("body").modal('hide');

            $('.addBankModal').appendTo("body").modal('hide');

            $('.filterModal').appendTo("body").modal('hide');
        };
        
        $scope.sanctionModal = ['Form 1','Annx 1','Bye Laws', 'Board Resolution','List of directors','Running Groups','Affidavit','Bank Guarantee','Draft Chit Agreement','Balance Sheet','Net Owned Funds Certificate'];

        $scope.registrationModalArray = ['Form 3','List of all Subscribers','Non Judicial(Stamp Paper)','Board Resolution', 'Chit Agreements'];

        $scope.commencementModalArray = ['Declaration','Affidavit (Stamp Paper)','Request letter'];

        $scope.completionModalArray = ['Form 8'];

        $scope.bool = true;
        $scope.toggleLeft = function(){
            if(angular.equals($scope.bool, true)){
                $('.md-sidenav-left').hide();
                $scope.bool = false;
            }
            // $mdSidenav(navID).toggle();
        };
        
        $scope.toggleRight = function () {
            if(angular.equals($scope.bool, false)){
                $('.md-sidenav-left').show();
                $scope.bool = true;
            }
        };

        $scope.isOpen = function() { return $mdSidenav('left').isOpen(); };
        

    }

})();