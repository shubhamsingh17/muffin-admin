/**
 * Created by chitra on 27/9/17.
 */
(function() {
    'use strict';
    angular
        .module('app')
        .controller('BanksController', BanksController);

    BanksController.$inject = ['$scope', '$state', '$timeout','$http','network','$localStorage'];

    function BanksController($scope, $state, $timeout, $http, network, $localStorage) {

        $scope.searchText = "";

        $scope.$on('$viewContentLoaded', function(event) {

            $scope.getBanks();
            //TODO
            // if($scope.indexScope.isUserLoggedIn()){
            //     console.log("User Already LoggedIn");
            //     $scope.updatePageData();
            // }else{
            //     console.log("User Not LoggedIn");
            //     $scope.indexScope.goToLoginPage();
            // }
        });

        $scope.banks = [];
        $scope.getBanks = function () {
            $scope.showProgress = true;
            network.getBanksApiCall()
                .then(function(res) {
                    // console.log(res.data);

                    if (res.data && res.data.status == 'success') {

                        $scope.banks = res.data.response;

                    } else if (res.data.status == 'error') {
                        swal("Oops...", res.data.errorInfo, "error");
                    }else{
                        swal("Oops...", "Unable to parse server response", "error");
                    }
                    $scope.showProgress = false;
                }, function(err) {
                    $scope.showProgress = false;
                    if(err.status === 401){
                        swal("Oops...", err.data.error, "error");
                    }else {
                        swal("Oops...", "Something went wrong. Please try again.", "error");
                    }
                    console.log(err);
                });
        };

        $scope.bank = {};
        $scope.bank.bankName = "";
        $scope.bank.ifscCode = "";
        $scope.bank.address = "";
        $scope.bank.telephone = "";
        $scope.bank.micrCode = "";
        $scope.bank.branch = "";

        $scope.addBank = function () {
            $scope.showaddBankProgress = true;
            // console.log("add bank" , $scope.bank);
            network.createBankApiCall($scope.bank)
                .then(function(res) {
                    // console.log(res.data);

                    if (res.data && res.data.status == 'success') {

                        $scope.getBanks();
                        $('.addBankModal').appendTo("body").modal('hide');
                        $scope.bank.bankName = "";
                        $scope.bank.ifscCode = "";
                        $scope.bank.address = "";
                        $scope.bank.telephone = "";
                        $scope.bank.micrCode = "";
                        $scope.bank.branch = "";

                    } else if (res.data.status == 'error') {
                        swal("Oops...", res.data.errorInfo, "error");
                    }else{
                        swal("Oops...", "Unable to parse server response", "error");
                    }
                    $scope.showaddBankProgress = false;
                }, function(err) {
                    $scope.showaddBankProgress = false;
                    if(err.status === 401){
                        swal("Oops...", err.data.error, "error");
                    }else {
                        swal("Oops...", "Something went wrong. Please try again.", "error");
                    }
                    console.log(err);
                });
        };

        $scope.editBank = function () {
            $scope.showEditProgress = true;
            // console.log("edit bank" , $scope.bank);
            network.editBankApiCall($scope.bank, $scope.bank._id)
                .then(function(res) {
                    // console.log(res.data);

                    if (res.data && res.data.status == 'success') {

                        $scope.getBanks();
                        $('.editBankModal').appendTo("body").modal('hide');
                        $scope.bank.bankName = "";
                        $scope.bank.ifscCode = "";
                        $scope.bank.address = "";
                        $scope.bank.telephone = "";
                        $scope.bank.micrCode = "";
                        $scope.bank.branch = "";

                    } else if (res.data.status == 'error') {
                        swal("Oops...", res.data.errorInfo, "error");
                    }else{
                        swal("Oops...", "Unable to parse server response", "error");
                    }

                    $scope.showEditProgress = false;
                }, function(err) {
                    $scope.showEditProgress = false;
                    if(err.status === 401){
                        swal("Oops...", err.data.error, "error");
                    }else {
                        swal("Oops...", "Something went wrong. Please try again.", "error");
                    }
                    console.log(err);
                });
        };


        $scope.showAddBankModal = function () {
            $scope.bank = {};
            $scope.bank.bankName = "";
            $scope.bank.ifscCode = "";
            $scope.bank.address = "";
            $scope.bank.telephone = "";
            $scope.bank.micrCode = "";
            $scope.bank.branch = "";
            $('.addBankModal').appendTo("body").modal('show');

        };

        $scope.showEditBankModal = function (index) {
            $scope.bank = $scope.banks[index];
            $('.editBankModal').appendTo("body").modal('show');

        };

        $scope.closeModal = function () {
            $('.addBankModal').appendTo("body").modal('hide');
            $('.editBankModal').appendTo("body").modal('hide');

        };

        $("#phone").keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                // Allow: Ctrl/cmd+A
                (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: Ctrl/cmd+C
                (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: Ctrl/cmd+X
                (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: home, end, left, right
                (e.keyCode >= 35 && e.keyCode <= 39)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });

        $("#editPhone").keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                // Allow: Ctrl/cmd+A
                (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: Ctrl/cmd+C
                (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: Ctrl/cmd+X
                (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: home, end, left, right
                (e.keyCode >= 35 && e.keyCode <= 39)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });

    }

})();