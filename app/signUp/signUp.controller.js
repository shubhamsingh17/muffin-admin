/**
 * Created by chitra on 4/10/17.
 */
(function() {
    'use strict';
    angular
        .module('app')
        .controller('SignUpController', SignUpController);

    SignUpController.$inject = ['$scope', '$state', '$timeout','$http','network','$localStorage'];

    function SignUpController($scope, $state, $timeout, $http, network, $localStorage) {

        $scope.signUp = {};
        $scope.signUp.name = "";
        $scope.signUp.username = "";
        $scope.signUp.email = "";
        $scope.signUp.password = "";
        $scope.signUp.phone = "";
        $scope.signUp.role = 'admin';
        
        $scope.adminTypeSelected = function (type) {
            $scope.signUp.role = type;
        };

        $scope.signUpClicked = function(){
            
            if($scope.signUp.name && $scope.signUp.name.length > 0){
                if($scope.signUp.username && $scope.signUp.username.length > 0){
                    if($scope.signUp.email && $scope.signUp.email.length > 0){
                        if($scope.signUp.password && $scope.signUp.password.length > 0){
                            if($scope.signUp.phone && $scope.signUp.phone.length > 0){
                                if($scope.signUp.role && $scope.signUp.role.length > 0){

                                    $scope.signUpApi();

                                }else {
                                    swal({
                                        title: 'Error!',
                                        text: "Please enter role",
                                        type: 'error',
                                        confirmButtonText: 'OK'
                                    });
                                }
                            }else {
                                swal({
                                    title: 'Error!',
                                    text: "Please enter phone",
                                    type: 'error',
                                    confirmButtonText: 'OK'
                                });
                            }

                        }else {
                            swal({
                                title: 'Error!',
                                text: "Please enter password",
                                type: 'error',
                                confirmButtonText: 'OK'
                            });
                        }

                    }else {
                        swal({
                            title: 'Error!',
                            text: "Please enter email",
                            type: 'error',
                            confirmButtonText: 'OK'
                        });
                    }
                }else {
                    swal({
                        title: 'Error!',
                        text: "Please enter username",
                        type: 'error',
                        confirmButtonText: 'OK'
                    });
                }
            }else {
                swal({
                    title: 'Error!',
                    text: "Please enter name",
                    type: 'error',
                    confirmButtonText: 'OK'
                });
            }

        };

        $scope.signUpApi = function () {

            console.log($scope.signUp);

            $scope.showProgress = true;
            network.signUpApiCall($scope.signUp)
                .then(function(res) {
                    console.log(res.data);

                    if (res.data && !res.data.isError) {

                        swal({
                            title: 'Success!',
                            text: "Successfully Logged in",
                            type: 'success',
                            confirmButtonText: 'OK'
                        });
                        
                        $localStorage.authToken = res.data.data.token;
                        $localStorage.userId = res.data.data.users._id;
                        $localStorage.email = res.data.data.users.email;
                        $localStorage.name = res.data.data.users.username;
                        $localStorage.role = res.data.data.users.role;

                        $scope.$parent.goToPipeline();

                    } else if (res.data.isError) {
                        swal("Oops...", res.data.error, "error");
                    }else{
                        swal("Oops...", "Unable to parse server response", "error");
                    }
                    $scope.showProgress = false;
                }, function(err) {
                    $scope.showProgress = false;
                    if(err.status === 401){
                        swal("Oops...", err.data.error, "error");
                    }else {
                        swal("Oops...", "Something went wrong. Please try again.", "error");
                    }
                    console.log(err);
                });
        };



    }

})();