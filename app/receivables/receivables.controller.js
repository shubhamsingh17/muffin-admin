/**
 * Created by chitra on 27/9/17.
 */
(function() {
    'use strict';
    angular
        .module('app')
        .controller('ReceivablesController', ReceivablesController);

    ReceivablesController.$inject = ['$scope', '$state', '$timeout','$http','network','$localStorage'];

    function ReceivablesController($scope, $state, $timeout, $http, network, $localStorage) {

        $scope.showFilter = function () {
            $('.filterModal').appendTo("body").modal('show');

        };

        $scope.closeFilterModal = function () {
            $('.filterModal').appendTo("body").modal('hide');

        };

        $scope.filter = function () {
            $('.filterModal').appendTo("body").modal('hide');

        };

        //Get All Available Muffins 
        $scope.allReceivables = function () {
            $scope.usersDueAmount = [];
            $scope.showMuffinProgress = true;
            network.getAvailableMuffinsApiCall()
                .then(function(res) {
                    console.log("getAvailableMuffinsApiCall", res.data);
                    if (res.data && res.data.status == 'success') {
                        
                        
                    } else if (res.data.status == 'error') {
                        swal("Oops...", res.data.error, "error");
                    }else{
                        swal("Oops...", "Unable to parse server response", "error");
                    }
                    $scope.showMuffinProgress = false;
                }, function(err) {
                    $scope.showMuffinProgress = false;
                    if(err.status === 401){
                        swal("Oops...", err.data.error, "error");
                    }else {
                        swal("Oops...", "Something went wrong. Please try again.", "error");
                    }
                });
        };




    }

})();