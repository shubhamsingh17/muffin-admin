/**
 * Created by chitra on 27/9/17.
 */

(function() {
    'use strict';
    angular
        .module('app')
        .controller('PipelineController', PipelineController);

    PipelineController.$inject = ['$scope', '$state', '$timeout','$http','network','$localStorage'];

    function PipelineController($scope, $state, $timeout, $http, network, $localStorage) {

        $scope.searchUser = "";
        
        // Muffins pipeline
        $scope.selectedPipeline = 'NewMuffin';
        $scope.piplineTabSelected = function (tab) {
            $scope.selectedPipeline = tab;
            
            // if(angular.equals($scope.selectedPipeline,'Approval')){
            //     $scope.availableMuffin();
            // }
        };

        $scope.subscriptionQueueMuffinSelected = function (i, muffin) {
            $scope.subscriptionQueueMuffinSelectedIndex = i;
            $scope.getAllUsersOfMuffin(muffin._id);
        };

        $scope.approvalMuffinSelected = function (i) {
            $scope.approvalMuffinSelectedIndex = i;
        };

        $scope.newMuffin = {};
        $scope.newMuffin.commission = "5";
        $scope.newMuffin.minPrizeAmt = "65";
        $scope.newMuffin.lateFee = "2";
        $scope.newMuffin.name = "";
        $scope.newMuffin.chitValue = 50000;
        $scope.newMuffin.members = 10;
        $scope.newMuffin.frequency = 10;
        $scope.newMuffin.instalment = 5000;
        $scope.newMuffin.createdByAdmin = $localStorage.userId;
        $scope.newMuffin.createdByAdminType = 'admin';

        $scope.durationSelected = function (duration) {
            $scope.newMuffin.members = duration;
            $scope.newMuffin.frequency = duration;
            $scope.newMuffin.chitValue = $scope.newMuffin.instalment * $scope.newMuffin.frequency;
        };
        
        $scope.instalmentSelected = function (instalment) {
            $scope.newMuffin.instalment = instalment;
            $scope.newMuffin.chitValue = $scope.newMuffin.instalment * $scope.newMuffin.frequency;
        };

        // add new muffin Api Call
        $scope.addNewMuffin = function () {
            if($scope.newMuffin.name && $scope.newMuffin.name.length > 0 ){
                $scope.addMuffinProgress = true;
                console.log($scope.newMuffin);

                network.addNewMuffinApiCall($scope.newMuffin)
                    .then(function(res) {
                        console.log(res.data);
                        if (res.data && !res.data.isError) {
                            swal({
                                title: 'Success!',
                                text: res.data.message,
                                type: 'success',
                                confirmButtonText: 'OK'
                            });

                            $scope.newMuffin.name = "";
                            $scope.availableMuffin();


                        } else if (res.data.isError) {
                            swal("Oops...", res.data.error, "error");
                        }else{
                            swal("Oops...", "Unable to parse server response", "error");
                        }
                        $scope.addMuffinProgress = false;
                    }, function(err) {
                        console.log(err);
                        $scope.addMuffinProgress = false;
                        if(err.status === 401){
                            swal("Oops...", err.data.error, "error");
                        }else {
                            swal("Oops...", "Something went wrong. Please try again.", "error");
                        }
                    });
            }else {
                swal({
                    title: 'Error!',
                    text: "Please enter Muffin Name",
                    type: 'error',
                    confirmButtonText: 'OK'
                });
            }
        };

        //Available Muffins Api Call
        $scope.availableMuffin = function () {
            $scope.approvalQueueMuffins = [];
            $scope.subscriptionQueueMuffins = [];
            $scope.previousSanctionQueueMuffins = [];
            $scope.registerQueueMuffins = [];
            $scope.commencementQueueMuffins = [];
            $scope.completionQueueMuffins = [];
            $scope.activeMuffins = [];

            $scope.showApprovalMuffinProgress = true;
            network.getAvailableMuffinsApiCall($localStorage.userId)
                .then(function(res) {
                    console.log("getAvailableMuffinsApiCall", res.data);
                    if (res.data && res.data.status == 'success') {

                        if(res.data.response.length > 0){
                            for(var i=0; i< res.data.response.length; i++){
                                if(angular.equals(res.data.response[i].state, 'approval_queue')){
                                    $scope.approvalQueueMuffins.push(res.data.response[i]);

                                }else if(angular.equals(res.data.response[i].state, 'subscription_queue')){
                                    $scope.subscriptionQueueMuffins.push(res.data.response[i]);

                                }else if(angular.equals(res.data.response[i].state, 'reg_queue_previous_sanction')){
                                    $scope.previousSanctionQueueMuffins.push(res.data.response[i]);

                                }else if(angular.equals(res.data.response[i].state, 'reg_queue_registraion')){
                                    $scope.registerQueueMuffins.push(res.data.response[i]);

                                }else if(angular.equals(res.data.response[i].state, 'reg_queue_commencement')){
                                    $scope.commencementQueueMuffins.push(res.data.response[i]);

                                }else if(angular.equals(res.data.response[i].state, 'reg_queue_completion')){
                                    $scope.completionQueueMuffins.push(res.data.response[i]);

                                }else if(angular.equals(res.data.response[i].state, 'bidding_active')){
                                    $scope.activeMuffins.push(res.data.response[i]);

                                }
                            }
                        }
                    } else if (res.data.status == 'error') {
                        swal("Oops...", res.data.error, "error");
                    }else{
                        swal("Oops...", "Unable to parse server response", "error");
                    }
                    $scope.showApprovalMuffinProgress = false;
                }, function(err) {
                    $scope.showApprovalMuffinProgress = false;
                    if(err.status === 401){
                        swal("Oops...", err.data.error, "error");
                    }else {
                        swal("Oops...", "Something went wrong. Please try again.", "error");
                    }
                });
        };

        $scope.availableMuffin();


        $scope.getMuffinUsers = function () {
            $scope.allUsers = [];
            $scope.showProgress = true;
            network.getMuffinUsersApiCall()
                .then(function(res) {
                    console.log(res.data);

                    if (res.data && res.data.status == 'success') {

                        $scope.allUsers = res.data.response;
                        

                    } else if (res.data.status == 'error') {
                        swal("Oops...", res.data.errorInfo, "error");
                    }else{
                        swal("Oops...", "Unable to parse server response", "error");
                    }
                    $scope.showProgress = false;
                }, function(err) {
                    $scope.showProgress = false;
                    if(err.status === 401){
                        swal("Oops...", err.data.error, "error");
                    }else {
                        swal("Oops...", "Something went wrong. Please try again.", "error");
                    }
                    console.log(err);
                });
        };
        
        $scope.approveMuffin = function (muffin, approval) {
            $scope.data = {};
            $scope.data.name = muffin.name;
            $scope.data.muffinId = muffin._id;
            $scope.data.state = "Approval";
            $scope.data.stateAcceptDecline = approval;
            $scope.data.craetedByAdminType = "admin";
            console.log($scope.data);

            $scope.showProgress = true;
            network.acceptDeclineApiCall($scope.data)
                .then(function(res) {
                    console.log("acceptDeclineApiCall", res.data);
                    if (res.data && !res.data.isError) {

                        swal({
                            title: 'Success!',
                            text: res.data.message,
                            type: 'success',
                            confirmButtonText: 'OK'
                        });
                        $scope.availableMuffin();

                    } else if (res.data.isError) {
                        swal("Oops...", res.data.error, "error");
                    }else{
                        swal("Oops...", "Unable to parse server response", "error");
                    }
                    $scope.showProgress = false;
                }, function(err) {
                    $scope.showProgress = false;
                    console.log(err);
                    if(err.status === 401){
                        swal("Oops...", err.data.error, "error");
                    }else {
                        swal("Oops...", "Something went wrong. Please try again.", "error");
                    }
                });
        };
        

        // Invite User
        $scope.inviteUser = function (userId) {
            $scope.data = {};
            $scope.data.userId = userId;
            $scope.data.relationType = "pending";
            $scope.data.muffinId = "";
            console.log($scope.data);

            $scope.showProgress = true;
            network.addInvitedMuffinApiCall($scope.data)
                .then(function(res) {
                    console.log("getAvailableMuffinsApiCall", res.data);
                    if (res.data && !res.data.isError) {

                        swal({
                            title: 'Success!',
                            text: res.data.message,
                            type: 'success',
                            confirmButtonText: 'OK'
                        });

                    } else if (res.data.isError) {
                        swal("Oops...", res.data.error, "error");
                    }else{
                        swal("Oops...", "Unable to parse server response", "error");
                    }
                    $scope.showProgress = false;
                }, function(err) {
                    $scope.showProgress = false;
                    console.log(err);
                    if(err.status === 401){
                        swal("Oops...", err.data.error, "error");
                    }else {
                        swal("Oops...", "Something went wrong. Please try again.", "error");
                    }
                });
        };
        
        $scope.inviteMuffin = function () {
            $('.inviteMuffinModal').appendTo("body").modal('show');
            $scope.getMuffinUsers();
            //TODO:
            $scope.data.muffinId = "";
        };
        
        // $scope.addNewMuffin = function () {
        //     $('.addNewMuffinModal').appendTo("body").modal('show');
        //
        // };

        $scope.closeModal = function () {
            
            $('.inviteMuffinModal').appendTo("body").modal('hide');
            $('.addNewMuffinModal').appendTo("body").modal('hide');
            $('.previousSanctionModal').appendTo("body").modal('hide');
            $('.registrationModal').appendTo("body").modal('hide');
            $('.commencementModal').appendTo("body").modal('hide');
            $('.completionModal').appendTo("body").modal('hide');

        };

        // get users of muffin api call
        $scope.getAllUsersOfMuffin = function (muffinId) {
            $scope.muffinUsers = [];
            $scope.showUsersProgress = true;

            network.getAllUsersOfMuffinApiCall(muffinId)
                .then(function(res) {
                    console.log("getAllUsersOfMuffinApiCall", res.data);
                    if (res.data && !res.data.isError) {

                        $scope.muffinUsers = res.data.data.others;

                    } else if (res.data.isError) {
                        swal("Oops...", res.data.error, "error");
                    }else{
                        swal("Oops...", "Unable to parse server response", "error");
                    }
                    $scope.showUsersProgress = false;
                }, function(err) {
                    $scope.showUsersProgress = false;
                    console.log(err);
                    if(err.status === 401){
                        swal("Oops...", err.data.error, "error");
                    }else {
                        swal("Oops...", "Something went wrong. Please try again.", "error");
                    }
                });
        };



    }

})();