/**
 * Created by chitra on 27/9/17.
 */

(function() {
    'use strict';
    angular
        .module('app')
        .controller('SalesController', SalesController);

    SalesController.$inject = ['$scope', '$state', '$timeout','$http','network','$localStorage','$mdpDatePicker'];

    function SalesController($scope, $state, $timeout, $http, network, $localStorage, $mdpDatePicker) {

        $scope.searchText = "";

        //Get Sales
        $scope.sales = [];
        $scope.getSales = function () {
            $scope.showProgress = true;
            network.getSalesApiCall()
                .then(function(res) {
                    console.log("sales ", res.data);

                    if (res.data && res.data.status == 'success') {
                        $scope.sales = res.data.response;

                    } else if (res.data.status == 'error') {
                        swal("Oops...", res.data.errorInfo, "error");
                    }else{
                        swal("Oops...", "Unable to parse server response", "error");
                    }
                    $scope.showProgress = false;
                }, function(err) {
                    $scope.showProgress = false;
                    if(err.status === 401){
                        swal("Oops...", err.data.error, "error");
                    }else {
                        swal("Oops...", "Something went wrong. Please try again.", "error");
                    }
                    console.log(err);
                });
        };

        $scope.getSales();

        // Add Sales
        $scope.sale = {};
        $scope.sale.name = "";
        $scope.sale.email = "";
        $scope.sale.phone = "";
        $scope.sale.address = "";
        $scope.sale.volumeExpected = "";
        $scope.sale.executive = $localStorage.userId;
        $scope.sale.valuePerPerson = "";
        $scope.sale.details = "";

        $scope.addSales = function () {
            $scope.showAddSalesProgress = true;
            console.log("add sales" , $scope.sale);
            network.addSalesApiCall($scope.sale)
                .then(function(res) {
                    console.log(res.data);

                    if (res.data && res.data.status == 'success') {
                        $scope.getSales();
                        $('.addSalesModal').appendTo("body").modal('hide');

                    } else if (res.data.status == 'error') {
                        swal("Oops...", res.data.errorInfo, "error");
                    }else{
                        swal("Oops...", "Unable to parse server response", "error");
                    }
                    $scope.showAddSalesProgress = false;
                }, function(err) {
                    $scope.showAddSalesProgress = false;
                    if(err.status === 401){
                        swal("Oops...", err.data.error, "error");
                    }else {
                        swal("Oops...", "Something went wrong. Please try again.", "error");
                    }
                    console.log(err);
                });
        };

        // Get Sales Task
        $scope.getSalesTasks = function (salesId) {
            $scope.salesTask = [];
            $scope.showTaskProgress = true;
            network.getSalesTasksApiCall(salesId)
                .then(function(res) {
                    console.log("getTasks Sales response",res.data);

                    if (res.data && !res.data.isError) {
                        $scope.salesTask = res.data.data;

                    } else if (res.data.isError) {
                        swal("Oops...", res.data.errorInfo, "error");

                    }else{
                        swal("Oops...", "Unable to parse server response", "error");
                    }
                    $scope.showTaskProgress = false;
                }, function(err) {
                    $scope.showTaskProgress = false;
                    if(err.status === 401){
                        swal("Oops...", err.data.error, "error");
                    }else {
                        swal("Oops...", "Something went wrong. Please try again.", "error");
                    }
                    console.log(err);
                });
        };

        // Add Sales task
        $scope.task = {};
        $scope.task.taskId = "";
        $scope.task.taskType = 'sales';
        $scope.task.taskDetails = "";
        $scope.task.taskSubject = "";
        $scope.task.createdBySales = "";
        $scope.task.taskDate = "";

        $scope.addTask = function () {
            $scope.showAddTaskProgress = true;
            console.log("add sales task" , $scope.task);
            network.addTaskApiCall($scope.task)
                .then(function(res) {
                    console.log(res.data);

                    if (res.data && res.data.status == 'success') {

                        $('.addTaskModal').appendTo("body").modal('hide');

                        $scope.getSalesTasks();
                        $scope.getSales();

                    } else if (res.data.status == 'error') {
                        swal("Oops...", res.data.errorInfo, "error");
                    }else{
                        swal("Oops...", "Unable to parse server response", "error");
                    }
                    $scope.showAddTaskProgress = false;
                }, function(err) {
                    $scope.showAddTaskProgress = false;
                    if(err.status === 401){
                        swal("Oops...", err.data.error, "error");
                    }else {
                        swal("Oops...", "Something went wrong. Please try again.", "error");
                    }
                    console.log(err);
                });
        };

        $scope.showAddSalesModal = function () {
            $('.addSalesModal').appendTo("body").modal('show');

        };
        $scope.closeAddSalesModal = function () {
            $('.addSalesModal').appendTo("body").modal('hide');

        };

        $scope.showTaskModal = function (saleId) {
            $('.taskModal').appendTo("body").modal('show');
            $scope.getSalesTasks(saleId);
            $scope.task.createdBySales = saleId;

        };
        $scope.closeTaskModal = function () {
            $('.taskModal').appendTo("body").modal('hide');

        };
        
        $scope.showAddTaskModal = function () {
            $('.addTaskModal').appendTo("body").modal('show');

        };
        $scope.closeAddTaskModal = function () {
            $('.addTaskModal').appendTo("body").modal('hide');
        };

        $scope.currentDate = new Date();
        $scope.selectDate = function(ev){

            var dateAndTime = "";
            $scope.task.taskDate="";
            $mdpDatePicker($scope.currentDate, {
                targetEvent: ev
            }).then(function(selectedDate) {

                dateAndTime = moment(selectedDate).format("DD-MM-YYYY");
                $scope.task.taskDate = dateAndTime;
                console.log(dateAndTime);

            });
        };
    }

})();