/**
 * Created by chitra on 9/10/17.
 */

(function() {
    'use strict';
    angular
        .module('app')
        .controller('AuctionsController', AuctionsController);

    AuctionsController.$inject = ['$scope', '$state', '$timeout','$http','network','$localStorage'];

    function AuctionsController($scope, $state, $timeout, $http, network, $localStorage) {

        $scope.searchText = "";

        $scope.auctions = [];
        $scope.getAuctions = function () {
            $scope.showProgress = true;
            network.getAuctionsApiCall()
                .then(function(res) {
                    console.log(res.data);

                    if (res.data && res.data.status == 'success') {
                        $scope.auctions = res.data.response;

                    } else if (res.data.status == 'error') {
                        swal("Oops...", res.data.errorInfo, "error");

                    }else{
                        swal("Oops...", "Unable to parse server response", "error");

                    }
                    $scope.showProgress = false;

                }, function(err) {
                    $scope.showProgress = false;
                    if(err.status === 401){
                        swal("Oops...", err.data.error, "error");
                    }else {
                        swal("Oops...", "Something went wrong. Please try again.", "error");
                    }
                    console.log(err);
                });
        };

        $scope.getAuctions();

    }

})();