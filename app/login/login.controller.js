/**
 * Created by chitra on 18/9/17.
 */
(function() {
    'use strict';
    angular
        .module('app')
        .controller('LoginController', LoginController);

    LoginController.$inject = ['$scope', '$state', '$timeout','$http','network','$localStorage'];

    function LoginController($scope, $state, $timeout, $http, network, $localStorage) {

        $scope.login = {};
        $scope.login.email = "";
        $scope.login.password = "";
        $scope.role = 'admin';

        console.log($scope.role);

        $scope.adminTypeSelected = function (type) {
            $scope.role = type;
        };

        $scope.login = function(){
            if($scope.login.email && $scope.login.email.length > 0){
                if($scope.login.password && $scope.login.password.length > 0){

                    $scope.loginApi();
                }else {
                    swal({
                        title: 'Error!',
                        text: "Please enter password",
                        type: 'error',
                        confirmButtonText: 'OK'
                    });
                }

            }else {
                swal({
                    title: 'Error!',
                    text: "Please enter EmailId.",
                    type: 'error',
                    confirmButtonText: 'OK'
                });
            }
        };

        $scope.loginApi = function () {
            $scope.data = {};
            $scope.data.email = $scope.login.email;
            $scope.data.password = $scope.login.password;
            $scope.data.role = $scope.role;
            console.log($scope.data);

            $scope.showProgress = true;
            network.loginApiCall($scope.data)
                .then(function(res) {
                    console.log(res.data);

                    if (res.data && !res.data.isError) {

                        swal({
                            title: 'Success!',
                            text: "Successfully Logged in",
                            type: 'success',
                            confirmButtonText: 'OK'
                        });

                        $localStorage.authToken = res.data.data.token;
                        $localStorage.userId = res.data.data.MuffinOfficeUsers._id;
                        $localStorage.email = res.data.data.MuffinOfficeUsers.email;
                        $localStorage.name = res.data.data.MuffinOfficeUsers.username;
                        $localStorage.role = res.data.data.MuffinOfficeUsers.role;

                        $scope.$parent.goToPipeline();

                    } else if (res.data.isError) {
                        swal("Oops...", res.data.error, "error");
                    }else{
                        swal("Oops...", "Unable to parse server response", "error");
                    }
                    $scope.showProgress = false;
                }, function(err) {
                    $scope.showProgress = false;
                    if(err.status === 401){
                        swal("Oops...", err.data.error, "error");
                    }else {
                        swal("Oops...", "Something went wrong. Please try again.", "error");
                    }
                    console.log(err);
                });
        };

        $scope.signUp = function () {
            $scope.goToSignUp();
        };


    }

})();