/**
 * Created by chitra on 27/9/17.
 */
(function() {
    'use strict';
    angular
        .module('app')
        .controller('LegalController', LegalController);

    LegalController.$inject = ['$scope', '$state', '$timeout','$http','network','$localStorage','$mdpDatePicker'];

    function LegalController($scope, $state, $timeout, $http, network, $localStorage, $mdpDatePicker) {

        $scope.searchText = "";

        $scope.$on('$viewContentLoaded', function(event) {

            $scope.getLegals();
            //TODO
            // if($scope.indexScope.isUserLoggedIn()){
            //     console.log("User Already LoggedIn");
            //     $scope.updatePageData();
            // }else{
            //     console.log("User Not LoggedIn");
            //     $scope.indexScope.goToLoginPage();
            // }
        });
        
        $scope.legals = [];
        $scope.getLegals = function () {
            $scope.showProgress = true;
            network.getLegalsApiCall()
                .then(function(res) {
                    console.log(res.data);

                    if (res.data && res.data.status == 'success') {

                        $scope.legals = res.data.response;

                    } else if (res.data.status == 'error') {
                        swal("Oops...", res.data.errorInfo, "error");
                    }else{
                        swal("Oops...", "Unable to parse server response", "error");
                    }
                    $scope.showProgress = false;
                }, function(err) {
                    $scope.showProgress = false;
                    if(err.status === 401){
                        swal("Oops...", err.data.error, "error");
                    }else {
                        swal("Oops...", "Something went wrong. Please try again.", "error");
                    }
                    console.log(err);
                });
        };

        $scope.legal = {};
        $scope.legal.courtOf = "";
        $scope.legal.sattelmentAmount = "";
        $scope.legal.location = "";
        $scope.legal.details = "";
        $scope.legal.userName = "";
        $scope.legal.user_id = "";

        $scope.addLegal = function () {
            $scope.showAddLegalProgress = true;
            console.log("add legal" , $scope.legal);
            network.addLegalApiCall($scope.legal)
                .then(function(res) {
                    console.log(res.data);

                    if (res.data && res.data.status == 'success') {

                        $scope.getLegals();
                        $('.addLegalModal').appendTo("body").modal('hide');


                    } else if (res.data.status == 'error') {
                        swal("Oops...", res.data.errorInfo, "error");
                    }else{
                        swal("Oops...", "Unable to parse server response", "error");
                    }
                    $scope.showAddLegalProgress = false;
                }, function(err) {
                    $scope.showAddLegalProgress = false;
                    if(err.status === 401){
                        swal("Oops...", err.data.error, "error");
                    }else {
                        swal("Oops...", "Something went wrong. Please try again.", "error");
                    }
                    console.log(err);
                });
        };

        $scope.muffinUsers = [];
        $scope.getMuffinUsers = function () {
            $scope.showProgress = true;
            network.getMuffinUsersApiCall()
                .then(function(res) {
                    console.log(res.data);

                    if (res.data && res.data.status == 'success') {

                        $scope.muffinUsers = res.data.response;
                        $scope.muffinUser = res.data.response[0].username;
                        

                    } else if (res.data.status == 'error') {
                        swal("Oops...", res.data.errorInfo, "error");
                    }else{
                        swal("Oops...", "Unable to parse server response", "error");
                    }
                    $scope.showProgress = false;
                }, function(err) {
                    $scope.showProgress = false;
                    if(err.status === 401){
                        swal("Oops...", err.data.error, "error");
                    }else {
                        swal("Oops...", "Something went wrong. Please try again.", "error");
                    }
                    console.log(err);
                });
        };
        
        $scope.userSelected = function (user) {
            $scope.muffinUser = user.username;

            $scope.legal.userName = user.username;
            $scope.legal.user_id = user._id;
        };

        
        // Task

        $scope.getTasks = function (legalId) {
            $scope.tasks = [];
            $scope.showTaskProgress = true;
            network.getTasksApiCall(legalId)
                .then(function(res) {
                    console.log("getTasks response",res.data);

                    if (res.data && !res.data.isError) {
                        $scope.tasks = res.data.data;

                    } else if (res.data.isError) {
                        swal("Oops...", res.data.errorInfo, "error");
                        
                    }else{
                        swal("Oops...", "Unable to parse server response", "error");
                    }
                    $scope.showTaskProgress = false;
                }, function(err) {
                    $scope.showTaskProgress = false;
                    if(err.status === 401){
                        swal("Oops...", err.data.error, "error");
                    }else {
                        swal("Oops...", "Something went wrong. Please try again.", "error");
                    }
                    console.log(err);
                });
        };
        $scope.task = {};
        $scope.task.taskId = "";
        $scope.task.taskType = 'legal';
        $scope.task.taskDetails = "";
        $scope.task.taskSubject = "";
        $scope.task.createdByLegals = "";
        $scope.task.taskDate = "";

        $scope.taskTypeSelected = function (type) {
            $scope.task.taskType = type;
        };
        
        $scope.addTask = function () {
            $scope.showAddTaskProgress = true;
            console.log("add legal task" , $scope.task);
            network.addTaskApiCall($scope.task)
                .then(function(res) {
                    console.log(res.data);

                    if (res.data && res.data.status == 'success') {

                        $scope.getLegals();
                        $scope.getTasks();
                        $('.addTaskModal').appendTo("body").modal('hide');


                    } else if (res.data.status == 'error') {
                        swal("Oops...", res.data.message, "error");
                    }else{
                        swal("Oops...", "Unable to parse server response", "error");
                    }
                    $scope.showAddTaskProgress = false;
                }, function(err) {
                    $scope.showAddTaskProgress = false;
                    if(err.status === 401){
                        swal("Oops...", err.data.error, "error");
                    }else {
                        swal("Oops...", "Something went wrong. Please try again.", "error");
                    }
                    console.log(err);
                });
        };

        $scope.currentDate = new Date();
        $scope.selectDate = function(ev){

            var dateAndTime = "";
            $scope.task.taskDate="";
            $mdpDatePicker($scope.currentDate, {
                targetEvent: ev
            }).then(function(selectedDate) {

                dateAndTime = moment(selectedDate).format("DD-MM-YYYY");
                $scope.task.taskDate = dateAndTime;
                console.log(dateAndTime);

            });
        };

        $scope.showAddLegalModal = function () {
            $scope.getMuffinUsers();
            $('.addLegalModal').appendTo("body").modal('show');

        };
        $scope.closeAddLegalModal = function () {
            $('.addLegalModal').appendTo("body").modal('hide');

        };

        
        $scope.showTaskModal = function (legalId) {
            $('.taskModal').appendTo("body").modal('show');
            $scope.getTasks(legalId);
            $scope.task.createdByLegals = legalId;
            
        };
        $scope.closeTaskModal = function () {
            $('.taskModal').appendTo("body").modal('hide');
        };

        $scope.showAddTaskModal = function () {
            $('.addTaskModal').appendTo("body").modal('show');

        };
        $scope.closeAddTaskModal = function () {
            $('.addTaskModal').appendTo("body").modal('hide');

        };





    }

})();