/**
 * Created by chitra on 24/10/17.
 */
(function() {
    'use strict';
    angular
        .module('app')
        .controller('ActiveController', ActiveController);

    ActiveController.$inject = ['$scope', '$state', '$timeout','$http','network','$localStorage'];

    function ActiveController($scope, $state, $timeout, $http, network, $localStorage) {

        //Available Muffins Api Call
        $scope.availableMuffin = function () {
            $scope.activeMuffins = [];

            $scope.showProgress = true;
            network.getAvailableMuffinsApiCall($localStorage.userId)
                .then(function(res) {
                    console.log("getAvailableMuffinsApiCall", res.data);
                    if (res.data && res.data.status == 'success') {

                        if(res.data.response.length > 0){
                            for(var i=0; i< res.data.response.length; i++){
                                if(angular.equals(res.data.response[i].state, 'bidding_active')){
                                    $scope.activeMuffins.push(res.data.response[i]);

                                }
                            }
                        }
                    } else if (res.data.status == 'error') {
                        swal("Oops...", res.data.error, "error");
                    }else{
                        swal("Oops...", "Unable to parse server response", "error");
                    }
                    $scope.showProgress = false;
                }, function(err) {
                    $scope.showProgress = false;
                    if(err.status === 401){
                        swal("Oops...", err.data.error, "error");
                    }else {
                        swal("Oops...", "Something went wrong. Please try again.", "error");
                    }
                });
        };

        $scope.availableMuffin();

        // get users of muffin api call
        $scope.getAllUsersOfMuffin = function (muffinId) {
            $scope.muffinUsers = [];
            $scope.showUsersProgress = true;

            network.getAllUsersOfMuffinApiCall(muffinId)
                .then(function(res) {
                    console.log("getAllUsersOfMuffinApiCall", res.data);
                    if (res.data && !res.data.isError) {

                        $scope.muffinUsers = res.data.data.others;

                    } else if (res.data.isError) {
                        swal("Oops...", res.data.error, "error");
                    }else{
                        swal("Oops...", "Unable to parse server response", "error");
                    }
                    $scope.showUsersProgress = false;
                }, function(err) {
                    $scope.showUsersProgress = false;
                    console.log(err);
                    if(err.status === 401){
                        swal("Oops...", err.data.error, "error");
                    }else {
                        swal("Oops...", "Something went wrong. Please try again.", "error");
                    }
                });
        };

        $scope.showMembers = function (muffinId) {
            $scope.getAllUsersOfMuffin(muffinId);
            $('.muffinMembersModal').appendTo("body").modal('show');
            
        };

        $scope.closeMuffinMembersModal = function () {
            $('.muffinMembersModal').appendTo("body").modal('hide');

        };
        

    }

})();