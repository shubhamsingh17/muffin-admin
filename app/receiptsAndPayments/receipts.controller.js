/**
 * Created by chitra on 27/9/17.
 */

(function() {
    'use strict';
    angular
        .module('app')
        .controller('ReceiptsController', ReceiptsController);

    ReceiptsController.$inject = ['$scope', '$state', '$timeout','$http','network','$localStorage'];

    function ReceiptsController($scope, $state, $timeout, $http, network, $localStorage) {

        $scope.receiptQueueSelected = function (i, muffin) {
            $scope.receiptQueueSelcetedIndex = i;
            $scope.getAllUsersOfMuffin(muffin._id);
        };

        $scope.bank = 'Banks';
        $scope.bankSelected = function (bank) {
            $scope.bank = bank.bankName;
        };

        $scope.muffin = 'Muffins';
        $scope.muffinId = "";

        $scope.requisitionMuffinSelected = function (muffin) {
            $scope.requisitionMuffinFaceValue = muffin.chitValue;
            $scope.requisitionMuffinInstalment = muffin.instalment;
            $scope.getAllUsersOfMuffin(muffin._id);
            $scope.getAmountPaidFee2();

            $scope.muffin = muffin.name;
            $scope.muffinId = muffin._id;
            console.log('muffinId',muffin._id);
        };

        $scope.getAmountPaidFee2 = function () {
            $scope.amountPaidFeeArray = [];
            network.getAmountPaidFeeApiCall()
                .then(function(res) {
                    console.log("amount paid fee",res.data);

                    if (res.data && res.data.status == 'success') {

                        if(res.data.response.length > 0){
                            for(var i=0; i < res.data.response.length; i++ ){
                                if(angular.equals(res.data.response[i].muffinId, $scope.muffinId)){

                                    $scope.amountPaidFeeArray.push(response[i].amountPaid);
                                }
                            }
                        }

                    } else if (res.data.status == 'error') {
                        swal("Oops...", res.data.status, "error");
                    }else{
                        swal("Oops...", "Unable to parse server response", "error");
                    }
                    $scope.showProgress = false;
                }, function(err) {
                    $scope.showProgress = false;
                    if(err.status === 401){
                        swal("Oops...", err.data.status, "error");
                    }else {
                        swal("Oops...", "Something went wrong. Please try again.", "error");
                    }
                });
        };

        $scope.individualReceiptMuffinSelected = function (muffin) {
            $scope.getAllUsersOfMuffin(muffin._id);

            $scope.muffin = muffin.name;
            $scope.muffinId = muffin._id;

        };
        
        $scope.user = "Users";
        $scope.userId = "";
        $scope.userSelected = function (user) {
            $scope.user = user.name;
            $scope.userId = user._id;

            $scope.getAllPayments();
        };

        // Get All Banks
        $scope.getBanks = function () {
            $scope.showBankProgress = true;
            $scope.banks = [];
            network.getBanksApiCall()
                .then(function(res) {
                    console.log(res.data);

                    if (res.data && res.data.status == 'success') {

                        $scope.banks = res.data.response;

                    } else if (res.data.status == 'error') {
                        swal("Oops...", res.data.errorInfo, "error");
                    }else{
                        swal("Oops...", "Unable to parse server response", "error");
                    }
                    $scope.showBankProgress = false;
                }, function(err) {
                    $scope.showBankProgress = false;
                    if(err.status === 401){
                        swal("Oops...", err.data.error, "error");
                    }else {
                        swal("Oops...", "Something went wrong. Please try again.", "error");
                    }
                    console.log(err);
                });
        };

        //Get All Available Muffins 
        $scope.availableMuffin = function () {
            $scope.availableMuffins = [];
            $scope.showMuffinProgress = true;
            network.getAvailableMuffinsApiCall($localStorage.userId)
                .then(function(res) {
                    console.log("getAvailableMuffinsApiCall", res.data);
                    if (res.data && res.data.status == 'success') {

                        if(res.data.response.length > 0){
                            for(var i=0; i < res.data.response.length; i++){
                                if(res.data.response[i].state && angular.equals(res.data.response[i].state,'bidding_active')){
                                    $scope.availableMuffins.push(res.data.response[i]);
                                }
                            }
                        }
                    } else if (res.data.status == 'error') {
                        swal("Oops...", res.data.error, "error");
                    }else{
                        swal("Oops...", "Unable to parse server response", "error");
                    }
                    $scope.showMuffinProgress = false;
                }, function(err) {
                    $scope.showMuffinProgress = false;
                    if(err.status === 401){
                        swal("Oops...", err.data.error, "error");
                    }else {
                        swal("Oops...", "Something went wrong. Please try again.", "error");
                    }
                });
        };

        // // Get All Users
        // $scope.getMuffinUsers = function () {
        //     $scope.showUserProgress = true;
        //     $scope.muffinUsers = [];
        //     network.getMuffinUsersApiCall()
        //         .then(function(res) {
        //             console.log(res.data);
        //
        //             if (res.data && res.data.status == 'success') {
        //                 $scope.muffinUsers = res.data.response;
        //
        //             } else if (res.data.status == 'error') {
        //                 swal("Oops...", res.data.errorInfo, "error");
        //
        //             }else{
        //                 swal("Oops...", "Unable to parse server response", "error");
        //
        //             }
        //             $scope.showUserProgress = false;
        //         }, function(err) {
        //             $scope.showUserProgress = false;
        //             if(err.status === 401){
        //                 swal("Oops...", err.data.error, "error");
        //             }else {
        //                 swal("Oops...", "Something went wrong. Please try again.", "error");
        //             }
        //             console.log(err);
        //         });
        // };
        
        //Get All Payments
        $scope.getPayments = function () {
            $scope.payments = [];
            $scope.data = {};
            $scope.data.userId = $localStorage.userId;
            network.getAllPaymentsApiCall($scope.data)
                .then(function(res) {
                    console.log(res.data);
                    if (res.data && res.data.status == 'success') {


                    } else if (res.data.status == 'error') {
                        swal("Oops...", res.data.errorInfo, "error");

                    }else{
                        swal("Oops...", "Unable to parse server response", "error");

                    }
                    $scope.showUserProgress = false;
                }, function(err) {
                    $scope.showUserProgress = false;
                    if(err.status === 401){
                        swal("Oops...", err.data.error, "error");
                    }else {
                        swal("Oops...", "Something went wrong. Please try again.", "error");
                    }
                    console.log(err);
                });
        };

        // Get users of muffin api call
        $scope.getAllUsersOfMuffin = function (muffinId) {
            $scope.muffinUsers = [];
            $scope.showUsersProgress = true;

            network.getAllUsersOfMuffinApiCall(muffinId)
                .then(function(res) {
                    console.log("getAllUsersOfMuffinApiCall", res.data);
                    if (res.data && !res.data.isError) {

                        $scope.muffinUsers = res.data.data.others;

                    } else if (res.data.isError) {
                        swal("Oops...", res.data.error, "error");
                    }else{
                        swal("Oops...", "Unable to parse server response", "error");
                    }
                    $scope.showUsersProgress = false;
                }, function(err) {
                    $scope.showUsersProgress = false;
                    console.log(err);
                    if(err.status === 401){
                        swal("Oops...", err.data.error, "error");
                    }else {
                        swal("Oops...", "Something went wrong. Please try again.", "error");
                    }
                });
        };
        
        $scope.selectedPayment = 'requisition';
        $scope.paymentTabSelected = function (tab) {
            $scope.selectedPayment = tab;
            if(angular.equals($scope.selectedPayment, 'individualReceipt')){
                $scope.getBanks();
                $scope.availableMuffin();

            }else if(angular.equals($scope.selectedPayment, 'payment')){
                $scope.getPayments();

            }else if(angular.equals($scope.selectedPayment, 'receipt')){
                $scope.availableMuffin();

            }else if(angular.equals($scope.selectedPayment, 'requisition')){
                $scope.availableMuffin();
                
            }
        };

        $scope.paymentTabSelected('requisition');


        //get late fee amount
        $scope.getLateFee = function () {
            $scope.lateFee = "";
            network.getLateFeeApiCall()
                .then(function(res) {
                    console.log("late fee",res.data);
                    if (res.data && res.data.status == 'success') {

                        console.log("usrId", $scope.userId);

                        if(res.data.response.length > 0){
                            for(var i=0; i < res.data.response.length; i++ ){
                                if(angular.equals(res.data.response[i].userId, $scope.userId) &&
                                    angular.equals(res.data.response[i].muffinId, $scope.muffinId)){

                                    $scope.lateFee = response[i].amountPaid;
                                }
                            }
                        }

                    } else if (res.data.status == 'error') {
                        swal("Oops...", res.data.status, "error");
                    }else{
                        swal("Oops...", "Unable to parse server response", "error");
                    }
                    $scope.showProgress = false;
                }, function(err) {
                    $scope.showProgress = false;
                    if(err.status === 401){
                        swal("Oops...", err.data.status, "error");
                    }else {
                        swal("Oops...", "Something went wrong. Please try again.", "error");
                    }
                });
        };

        //get bounce fee amount
        $scope.getBounceFee = function () {
            $scope.bounceFee = "";
            network.getBounceFeeApiCall()
                .then(function(res) {
                    console.log("bounce fee",res.data);
                    if (res.data && res.data.status == 'success') {

                        if(res.data.response.length > 0){
                            for(var i=0; i < res.data.response.length; i++ ){
                                if(angular.equals(res.data.response[i].userId, $scope.userId) &&
                                    angular.equals(res.data.response[i].muffinId, $scope.muffinId)){

                                    $scope.bounceFee = response[i].amountPaid;
                                }
                            }
                        }
                    } else if (res.data.status == 'error') {
                        swal("Oops...", res.data.status, "error");
                    }else{
                        swal("Oops...", "Unable to parse server response", "error");
                    }
                    $scope.showProgress = false;
                }, function(err) {
                    $scope.showProgress = false;
                    if(err.status === 401){
                        swal("Oops...", err.data.status, "error");
                    }else {
                        swal("Oops...", "Something went wrong. Please try again.", "error");
                    }
                });
        };

        //get Delayed Payment fee amount
        $scope.getDelayedPaymentFee = function () {
            $scope.delayedPaymentFee = "";
            network.getDelayedPaymentFeeApiCall()
                .then(function(res) {
                    console.log("delayed payment fee",res.data);
                    if (res.data && res.data.status == 'success') {

                        if(res.data.response.length > 0){
                            for(var i=0; i < res.data.response.length; i++ ){
                                if(angular.equals(res.data.response[i].userId, $scope.userId) &&
                                    angular.equals(res.data.response[i].muffinId, $scope.muffinId)){

                                    $scope.delayedPaymentFee = res.data.response[i].amountPaid;
                                }
                            }
                        }

                    } else if (res.data.status == 'error') {
                        swal("Oops...", res.data.status, "error");
                    }else{
                        swal("Oops...", "Unable to parse server response", "error");
                    }
                    $scope.showProgress = false;
                }, function(err) {
                    $scope.showProgress = false;
                    if(err.status === 401){
                        swal("Oops...", err.data.status, "error");
                    }else {
                        swal("Oops...", "Something went wrong. Please try again.", "error");
                    }
                });
        };

        //get Instalment Payable fee amount
        $scope.getInstalmentPayableFee = function () {
            $scope.instalmentPayableFee = "";
            network.getInstalmentPayableFeeApiCall()
                .then(function(res) {
                    console.log("instalment payable fee",res.data);
                    if (res.data && res.data.status == 'success') {

                        if(res.data.response.length > 0){
                            for(var i=0; i < res.data.response.length; i++ ){
                                if(angular.equals(res.data.response[i].userId, $scope.userId) &&
                                    angular.equals(res.data.response[i].muffinId, $scope.muffinId)){

                                    $scope.instalmentPayableFee = res.data.response[i].amountPaid;
                                }
                            }
                        }

                    } else if (res.data.status == 'error') {
                        swal("Oops...", res.data.status, "error");
                    }else{
                        swal("Oops...", "Unable to parse server response", "error");
                    }
                    $scope.showProgress = false;
                }, function(err) {
                    $scope.showProgress = false;
                    if(err.status === 401){
                        swal("Oops...", err.data.status, "error");
                    }else {
                        swal("Oops...", "Something went wrong. Please try again.", "error");
                    }
                });
        };

        //get Amount Paid fee amount
        $scope.getAmountPaidFee = function () {
            $scope.amountPaidFee = "";
            network.getAmountPaidFeeApiCall()
                .then(function(res) {
                    console.log("amount paid fee",res.data);
                    if (res.data && res.data.status == 'success') {

                        if(res.data.response.length > 0){
                            for(var i=0; i < res.data.response.length; i++ ){
                                if(angular.equals(res.data.response[i].userId, $scope.userId) &&
                                    angular.equals(res.data.response[i].muffinId, $scope.muffinId)){

                                    $scope.amountPaidFee = response[i].amountPaid;
                                }
                            }
                        }

                    } else if (res.data.status == 'error') {
                        swal("Oops...", res.data.status, "error");
                    }else{
                        swal("Oops...", "Unable to parse server response", "error");
                    }
                    $scope.showProgress = false;
                }, function(err) {
                    $scope.showProgress = false;
                    if(err.status === 401){
                        swal("Oops...", err.data.status, "error");
                    }else {
                        swal("Oops...", "Something went wrong. Please try again.", "error");
                    }
                });
        };

        $scope.getAllPayments = function () {
            $scope.getLateFee();
            $scope.getBounceFee();
            $scope.getDelayedPaymentFee();
            $scope.getInstalmentPayableFee();
            $scope.getAmountPaidFee();
        };



    }

})();